// jquery loaded, restore original environment
if (window.module) {
    module = window.module;
}

// init toucha
(function($) {

// toucha entry points
let toucha_home_id      = 'toucha-homepage';
let toucha_icon_class   = 'toucha-app-icon';
let toucha_icon_data    = 'toucha-app-page';
let toucha_title_class  = 'toucha-title';
let toucha_title_data   = 'toucha-title';

// add apps place holder
// embeded app receives 4 page events
//  - pagecreate
//  - pagecontainerbeforeshow
//  - pagecontainershow
//  - pagecontainerbeforehide
let toucha = {
    page: '',   // current page id
    apps: {}    // embeded apps place holder, key is its page id
};

// embeded app factory, call this to register page app
$.fn.toucha = function(page, app) {
    if (page 
        && typeof page === 'string' 
        && typeof app === 'object'
        && typeof toucha.apps[page] === 'undefined'
    ) {
        toucha.apps[page] = app;
    }
};

// prepare for loading jquery mobile, then
// route page events to embeded app with page id
$(document).on("mobileinit", function () {
    $.mobile.defaultPageTransition = 'none';
}).on('pagecreate pagecontainerbeforechange pagecontainerbeforeshow pagecontainershow pagecontainerbeforehide', function (event, ui) {
    let page_id = event.target.id || '';
    let pre_page_id = (ui.prevPage && ui.prevPage.prop('id')) || '';
    let to_page_id = (ui.toPage && (typeof ui.toPage == 'string' ? ui.toPage : ui.toPage.prop('id'))) || '';
    let event_type = event.type;

    // update global settings
    if (event_type === 'pagecreate') {
        toucha.page = page_id;
    } else if (event_type === 'pagecontainerbeforechange') {
        toucha.page = to_page_id;
    } else if (event_type === 'pagecontainerbeforeshow') {
        toucha.page = to_page_id;
        // set title with this app data
        $('.' + toucha_title_class).text($('#' + toucha.page).data(toucha_title_data));
    } else if (event_type === 'pagecontainershow') {
        toucha.page = to_page_id;
    } else if (event_type === 'pagecontainerbeforehide') {
        toucha.page = pre_page_id;
        // clear title
        $('.' + toucha_title_class).text('');
    }

    // route events to embeded app
    let app = toucha.apps[toucha.page];
    if (typeof app === 'object' && typeof app[event_type] === 'function') {
        app[event_type].apply(app, [event, ui, pre_page_id, to_page_id]);
    }
}).on('click', function (event) {
    // embeded app starting effect, like ios
    let ts = $(event.target).closest('.' + toucha_icon_class);
    if (ts.length > 0) {
        let pc = $('#' + toucha_home_id);
        let pc_offset = pc.offset();
        let ts_offset = ts.offset();
        let pc_x = pc_offset.left;
        let pc_y = pc_offset.top;
        let pc_w = pc.width();
        let pc_h = pc.height();
        let ts_x = ts_offset.left;
        let ts_y = ts_offset.top;
        let ts_w = ts.outerWidth();
        let ts_h = ts.outerHeight();
        let zm_x = pc_w/ts_w;
        let zm_y = pc_h/ts_h;
        let dt_x = zm_x*((pc_x+0.5*pc_w)-(ts_x+0.5*ts_w));
        let dt_y = zm_y*((pc_y+0.5*pc_h)-(ts_y+0.5*ts_h));
        pc.css({
            'transition': '0.5s',
            'transform': 'matrix(' + [zm_x, 0.0, 0.0, zm_y, dt_x, dt_y].join(',') + ')'
        }).one('transitionend ', function (evt) {
            $(':mobile-pagecontainer').pagecontainer('change', ts.data(toucha_icon_data), {transition: 'fade'});
        });
    }
});

// register home page app
$.fn.toucha(toucha_home_id, {
    pagecontainershow: function(event, ui, pre_page_id, to_page_id) {
        let pc = $('#' + toucha_home_id);
        pc.css({'transform': 'none', 'min-height': pc.css('min-height')});
    }
});

})(jQuery);